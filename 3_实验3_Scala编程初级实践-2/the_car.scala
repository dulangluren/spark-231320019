/** 
2.设计一个Car类，其中包含了汽车的基本属性和方法，
    如车牌号、品牌、颜色、价格和驾驶、停车、加油等方法。
    然后创建了一个ElectricCar子类，
    其中除了继承了Car类的基本属性和方法外，
    还包含了电池容量和续航里程属性以及充电和驾驶方法。
    同时，需要重写了drive方法，
    在驾驶电动汽车时会检查电池续航里程是否足够，如果不够就无法驾驶。
    （继承关系必须使用）
**/

class Car(val license_plate_number: String, val brand: String, val color: String, val price: Double) {
    def drive(mileage: Int): Unit = {print("this drive")}
    def stop(): Unit = {print("this stop")}
    def refuel(): Unit = {print("this refuel")}
}

class ElectricCar(license_plate_number: String, brand: String, color: String, price: Double, val battery_capacity: Double, val endurance_mileage: Double) extends Car(license_plate_number, brand, color, price) {
    def charge(): Unit = {print("this is charge")}
    override def drive(endurance_mileage: Int): Unit = {
        if(endurance_mileage <= 0) {
            println("you can not drive")
        }
    }

}