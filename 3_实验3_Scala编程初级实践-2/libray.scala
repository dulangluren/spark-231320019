/**
1.设计一个图书管理系统，其中包括Book类和Library类。
    Book类应该包括书名、作者和ISBN等属性，
    并且能够实现借出和归还书籍的方法。
    Library类应该包括一个可用书籍列表和一个已借出书籍列表，
    以及能够添加新书、借出书籍和归还书籍的方法。
    （apply、unapply方法必须使用）
**/

class Book(var titleOfBook: String, var author: String, var isbn: String) {
    def lend(): Unit = {}
    def return_(): Unit = {}
}

object Book {
    def apply(titleOfBook: String, author: String, isbn: String) = new Book(titleOfBook, author, isbn)
    def unapply(b: Book): Unit = {
        print("i donot know what is this: %s,%s, %s",b.titleOfBook,b.author,b.isbn)
    }
}

class Library(var availableBook: List[String], var booksLoan: List[String]) {
    def addNewBook():Unit = {}
    def loanBook(): Unit = {}
    def returnBook(): Unit = {}
}