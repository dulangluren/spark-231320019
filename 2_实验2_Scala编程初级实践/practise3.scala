// 编写一个函数，接收一个整数列表，将其中所有的偶数转换为它们的一半，将所有的奇数加上1，并返回新的列表。（practise3.scala）

// 闭包
def transformList(list: List[Int]): List[Int] = {
    return list.map( x => if(x % 2 == 0) x/2 ;else x+1);
}

println(transformList(List(1,2,4,6,7,65,34,2,34,5,76,5,234,2,65,6)))