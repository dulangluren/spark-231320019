// 编写一个函数，接收一个整数列表和一个整数n，返回其中所有小于n的数的平均值。（practise1.scala）

// 循环
def averageOfListBelowN(list: List[Int], n: Int): Double = {
    var average : Double = 0;
    var num : Int = 0;
    for(i <- list ) {
        if(i < n) {
            average += i;
            num += 1;
        }
    }
    if(num == 0) return 0;
    else return average/num;
}

// 暂时不会的过滤
def averageOfListBelowN2(list: List[Int], n: Int): Double = {
  val filteredList = list.filter(_ < n)
  if (filteredList.isEmpty) {
    return 0
  } else {
    return filteredList.sum.toDouble / filteredList.length.toDouble
  }

println(averageOfListBelowN(List(12,22,44,6,7,8,34,5,6,57,23,4,66,23), 10));
// println(averageOfListBelowN2(List(12,22,44,6,7,8,34,5,6,57,23,4,66,23), 10))
