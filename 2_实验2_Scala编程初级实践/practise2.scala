// 编写一段Scala代码实现判断一个字符串是否为回文字。（practise2.scala）

def isPalindrome(str: String): Boolean = {
    val str_rev = str.reverse;
    for(i <- 1 to str.length/2) {
        if(str(i)!=str_rev(i)) return false;
    }
    return true;
}

def isPalindrome2(str: String): Boolean = {
  val len = str.length
  val halfLen = len / 2
  for (i <- 0 until halfLen) {
    if (str(i) != str(len - i - 1)) {
      return false
    }
  }
  return true
}

// 测试样例
println(isPalindrome("racecar")) // true
println(isPalindrome("hello")) // false