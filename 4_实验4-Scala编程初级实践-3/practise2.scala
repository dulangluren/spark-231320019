/**
* 定义一个 Calculator 类，该类应该具有一个calculate方法，
* 该方法接收两个整数和一个字符作为参数，表示需要执行的操作符。
* calculate方法应该根据操作符执行相应的计算，并返回结果。
* 如果操作符无效，calculate 方法应该返回一个字符串 'Invalid operator'.
* （需要使用match语句）
**/

/**
* 在 scala 中 ?
* 单引号 ' 表示字符类型
* 双引号 '' 表示字符串类型
**/

// match - case
class Calculator() {
    def calculate(x: Int, y: Int, operator:Char): Any = operator match {
        case '+' => return (x+y)
        case '-' => return (x-y)
        case '*' => return (x*y)
        case '/' => return (x/y)
        case '%' => return (x%y)
        case _ => return ("Invalid operator")
    }
}

val calculator = new Calculator()
println(calculator.calculate(10, 5, '+')) //15
println(calculator.calculate(10, 5, '-')) //5
println(calculator.calculate(10, 5, '*')) //50
println(calculator.calculate(10, 5, '/')) //2
println(calculator.calculate(10, 5, '%')) //0
println(calculator.calculate(10, 5, 'i')) //输出'Invalid operator'