/**
* 定义一个特质 Logger，该特质包含一个具体方法 log(message: String): Unit。该方法将消息记录到日志中。
* 定义一个类 Calculator，它扩展 Logger 特质。 Calculator 类具有两个具体方法 add(x: Int, y: Int): Int 
*   和 subtract(x: Int, y: Int): Int，它们分别返回 x 和 y 的和或差。
* 在 add 和 subtract 方法内部，分别使用 log 方法记录所执行的操作和结果。
**/
trait Logger {
    def log(message: String): Unit = {}
}

class Calculator() extends Logger{
    def add(x: Int, y: Int): Unit = {
        val result = x + y
        val recode = s"Adding $x and $y. Result: $result"
        log(recode)
        println(recode)
    }
    def subtract(x: Int, y: Int): Unit = {
        val result = x - y
        val recode = s"Subtracting $y from $x. Result: $result"
        log(recode)
        println(recode)
    }
}

val calculator = new Calculator()
calculator.add(10,5) //Adding 10 and 5.Result:15"
calculator.subtract(10,5) //"Subtracting 5 from 10.Result:5"