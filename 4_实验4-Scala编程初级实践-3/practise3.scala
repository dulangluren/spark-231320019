/**
* 假设你正在编写一个程序来处理一组整数。你需要实现以下三个函数：
*   addOne 函数：接受一个整数列表作为参数，将列表中的每个整数加 1，并返回结果列表。
*   filterEven 函数：接受一个整数列表作为参数，返回所有偶数的列表。
*   mapReduce 函数：接受一个整数列表和两个函数作为参数，分别是映射函数和归约函数，
        该函数首先将列表中的每个元素应用于映射函数，然后将结果应用于归约函数并返回最终结果。
* 要求：
*   addOne 和 filterEven 函数必须使用高阶函数实现。
* 你可以使用任何你喜欢的 Scala 标准库函数来实现这些函数。
**/

def addOne(lis: List[Int]): List[Int] = {
    return lis.map(_ + 1)
}

def filterEven(lis: List[Int]): List[Int] = {
    return lis.filter(_ % 2 == 0)
}

def mapReduce(lis: List[Int], mapFun: Int => Int, reduceFun: (Int, Int) => Int) = {
    lis.map(mapFun).reduce(reduceFun)
}

val list = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
val addoneList  = addOne(list)
println(addoneList) //List(2,3,4,5,6,7,8,9,10,11)
val evenList = filterEven(list)
println(evenList) //输出List(2,4,6,8,10)
val snm = mapReduce(list, (x => x * 2), (_ + _))
println(snm) //输出110